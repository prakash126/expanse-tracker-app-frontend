
import React, { Fragment, useEffect, useState } from "react";

import {accountDetailsBasedOnAccountId,getUser} from '../../auth/helper/dashboardHelper';

import { Table } from "reactstrap";

const AccountDetails = (props) => {
    console.log(props.accountId)
  const [accountDetail, setAccountDetail] = useState("");
  const [currentUser, setCurrentUser] = useState("");

  useEffect(() => {
    accountDetailsBasedOnAccountId({id:props.accountId}).then(({ data }) => {
        console.log(data)
      setAccountDetail(data.account);
      getUser({id:data.account.userId}).then(({ data }) => {
        console.log(data.user)
      setCurrentUser(data.user);
    });
    });
   
  }, []);

  return (
    <div>
      <div className="container">
        <Table bordered={false}>
          <tbody>
            <tr>
              <th scope="row">Account Name</th>
              <td>
                {accountDetail.name ? accountDetail.name : "Fetching.."}
              </td>
            </tr>
            <tr>
              <th scope="row">Owner</th>
              <td>
                {accountDetail.userId === currentUser._id
                  ? currentUser.name
                  : "Fetching.."}
              </td>
            </tr>
            <tr>
              <th scope="row">AddedUser</th>
                  {accountDetail.addUser === undefined?<Fragment>
                   <td>No USer Added</td>
                  </Fragment>:<Fragment>
                      <td>{accountDetail.addUser}</td>
                    </Fragment>}
            </tr>
          </tbody>
        </Table>
      </div>
     
      
    </div>
  );
};

export default AccountDetails;
