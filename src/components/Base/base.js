import React from 'react';
import { Container } from 'reactstrap';
import Navbar from '../../views/navbar'
const Base = ({
    title="",
    description = "",
    className="bg-dark text-white p-4",
    children
})=> {
  return (
    <div>
        <Navbar />
        <Container fluid={true}>
                <div className="jumbotron bg-dark text-white text-center">
                    <h2 className="display-4">{title}</h2>
                    <p className="lead">{description}</p>
                    <div className={className} style={{height:"600px",zIndex:"1"}}>
                      {children}
                    </div>
                </div>
               
                <footer className="bg-dark mb-2 position-fixed">
                  
                  <div className="bg-success" style={{height:"50px"}}>
                          <span className="text-dark text-center mt-2">
                              An Amazing <span className="text-white">Expanse Manages App</span>&nbsp;Project
                          </span>
                  </div>
           </footer>
        </Container>
    </div>
  );
}

export default Base;