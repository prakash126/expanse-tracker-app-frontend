import React,{useState} from 'react';
import { Redirect,Link } from 'react-router-dom';

import "../../../src/styles.css";
import Base from '../Base/base';

const Home = ()=>{
    const redirectToSignIn = ()=>{
        console.log("his")
        return <Redirect to="/signin" />
    }
 return(
    <Base title="Welcome To Expanse Managing App" >
         <h1 className="text-center text-white"></h1>
         
         <Link to="/signin" className="btn btn-primary btn-lg m-5">
                Continue to Login
         </Link>
         <h4>If You don't have account? Signup Below</h4>
         <Link to="/signup" className="btn btn-primary btn-lg m-5">
                Continue to Signup
         </Link>
    </Base>
 )
}
 
export default Home;