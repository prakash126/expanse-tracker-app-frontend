import React, { useState,useEffect } from 'react'
import { Container, FormGroup, Jumbotron, Label,Row,Col,Table,Button } from 'reactstrap'
import Base from '../Base/base'
import {accountDetails,addUserToAccountByEmail,getAddedUser} from '../../auth/helper/dashboardHelper'
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const AddUser = (props) => {

    const [accounts, setAccounts] = useState([]);
    const [currentAccount, setCurrentAccount] = useState("");
    const [userEmail, setUserEmail] = useState("");
    const [addedUser, setAddedUser] = useState([]);
    let userId = "";
    let useremail="";
    useEffect(() => {
        const userdata = localStorage.getItem('userDetails');
        const udata = JSON.parse(userdata);
         userId = udata._id;
        accountDetails({ userId }).then(({ data }) => {
          // console.log(data)
            setAccounts(data);
            setCurrentAccount(data.length > 0 ? data[0]._id : "");
        }).catch((err)=>{
            console.log(err);
        })

        getAddedUser({userId})
        .then(({data})=>{
            //console.log(data.UserData)
            setAddedUser(data.UserData);
        }).catch((err)=>{
            console.log(err);
        })
    }, []);
    
    const saveData = ()=>{
        if(userEmail === null || userEmail === ""){
           return toast("Fields Shouldn't be empty!! Fill it!", {
                type: "error"
              });
        }
        else{
            const usdata = localStorage.getItem('userDetails');
            const data = JSON.parse(usdata);
            useremail=data.email;        
           console.log(userEmail);
           console.log(useremail);
           if(userEmail === useremail){
                    console.log("hi")
                return toast("You are Your own gmail into your account!! Not Added! Don't Do this", {
                        type: "error"
                    });
            }
            const obj = {
                accountId:currentAccount,
                userEmail:userEmail
            }
            addUserToAccountByEmail({obj}).then(({data})=>{
                //console.log(data)
                if(data.status === "error"){
                    toast(data.msg, {
                        type: "error"
                      });
                }
            }).catch((err)=>{
                console.log(err);
            })
        }
    }
    return (
        <>
            <Base title="Add User to Account">
                <ToastContainer></ToastContainer>
                <Container fluid className="mt-5 text-dark">
                    <Row>
                        <Col md="6">
                            <Jumbotron className="p-3">
                                <h2 className="">Add User</h2>
                                <hr className="my-1"></hr>
                                <FormGroup>
                                    <Label>Select Account</Label>
                                    {accounts.length === 0 ? (
                                        <div className="text-center text-danger">
                                            No accounts added!
                                        </div>
                                    ) : (
                                        <select className="form-control" value={currentAccount} onChange={(e)=>{
                                            setCurrentAccount(e.target.value)
                                        }}>
                                            {accounts.map((e,i)=>{
                                                return(
                                                    <option key={i} value={e._id}>
                                                        {e.name}
                                                    </option>
                                                )
                                            })}
                                        </select>
                                    )}
                                </FormGroup>
                                <FormGroup>
                                    <Label>Email of other User</Label>
                                    <input
                                        type="email"
                                        className="form-control"
                                        placeholder="Email"
                                        value={userEmail}
                                        onChange={(e) => {
                                          setUserEmail(e.target.value);
                                        }}                                    
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Button block onClick={saveData} color="primary">
                                        Save
                                    </Button>
                                </FormGroup>
                            </Jumbotron>
                        </Col>
                        <Col md="6">
                                <Jumbotron className="p-3">
                                    <h2>Added User</h2>
                                    <hr className="my-1" />
                                    <Table responsive>
                                    <thead>
                                        <tr>
                                        <th>Account</th>
                                        <th>User</th>
                                        <th>AdddedUser</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {addedUser.length === 0 ? (
                                        <tr className="text-center text-danger">
                                            <td colSpan="2">No User Found</td>
                                        </tr>
                                        ) : (
                                      
                                        addedUser.map((e, i) => {
                                            return (
                                                e.addedUser.map((elem,i)=>{
                                                    return(
                                                        <tr key={i}>
                                                        <td>{e.accountName}</td>
                                                        <td>{e.userName}</td>
                                                        <td>{elem}</td>
                                                    </tr>
                                                    )
                                                })
                                           
                                            );
                                        })
                                        )}
                                    </tbody>
                                    </Table>
                                </Jumbotron>
                         </Col>
                    </Row>
                </Container>
            </Base>
        </>
    )
}

export default AddUser;;