import React, { Component, Fragment } from 'react'
import Base from '../Base/base';
import { Row, Col, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import { getTransactionsDetails,editTransactionsDetails,deleteTransactionsDetails,accountDetails,getTransDetlsBsdOnAcNameAndUid} from '../../auth/helper/dashboardHelper';

let userId = '';
class listofAllTransaction extends Component {
    
    constructor(props) {
        super();
        this.state = {
            transactionDetails: [
                {
                    name: "",
                    balance: "",
                    id: ""
                }
            ],
            accountDetails:[],
            title:"All Transactions",
            accountName:'',
            isModalOpen: false,
            modalData: [
                {
                    input: "",
                    id: '',
                    header: '',
                    type: ''
                }],

        }
    }
    componentDidMount() {
        const userdata = localStorage.getItem('userDetails');
        const udata = JSON.parse(userdata);
         userId = udata._id

        // console.log(userId)
        getTransactionsDetails({ userId }).then(( {data} ) => {
           // console.log(data)
           
            this.setState({
                transactionDetails: data
            })
        
        });
        accountDetails({userId}).then(({data})=>{
            this.setState({
                accountDetails:data
            })
        })

        

    }
     getConvertedDate = (iso) => {
        const date = new Date(iso);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let dt = date.getDate();
    
        if (dt < 10) {
          dt = "0" + dt;
        }
        if (month < 10) {
          month = "0" + month;
        }
        return dt + "/" + month + "/" + year;
      };

       formatAMPM = (date) => {
        date = new Date(date);
        let hours = date.getHours();
        let minutes = date.getMinutes();
        let ampm = hours >= 12 ? "PM" : "AM";
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? "0" + minutes : minutes;
        let strTime = hours + ":" + minutes + " " + ampm;
        return strTime;
      };
    
    toggleModal = (type, id = null, name = null) => {
         if (type === "edit") {
            this.setState({
                modalData: {
                    type: "edit",
                    id: id,
                    header: "Edit Transaction Amount",
                    input: name
                }

            })
        }
        this.setState({ isModalOpen: !this.state.isModalOpen });
    }
    saveData = () => {
        //console.log(this.state.modalData)
       if(this.state.modalData.type === "edit"){
             var obj = {
                 id:this.state.modalData.id,
                 amount:this.state.modalData.input
             }

            // console.log(obj)
           
            editTransactionsDetails({obj}).then(({data})=>{
                console.log(data);
                window.location.reload();
            })
         }
    }

    fetchTransactionDetails=()=>{
        console.log(this.state.accountName)
        this.setState({title:`${this.state.accountName} Account Transaction Details`});
        if(this.state.accountName !==""){
            var obj = {
                accountName:this.state.accountName,
                userId:userId
            }
            getTransDetlsBsdOnAcNameAndUid({obj}).then(({data})=>{
                this.setState({
                    transactionDetails:data
                })
            })
        }
        else{
            this.setState({accountName:''})
        }
    }

    renderTableData() {
        //console.log(this.state.accountName)
        if(this.state.transactionDetails && this.state.transactionDetails.length>0){
            return (this.state.transactionDetails.map((data, index) => {
                //console.log(data)
                // const {name,balance } = data //destructuring
                return (
                    <tr key={index}>
                        <td>{index+1}</td>
                        <td>{data.amount}</td>
                        <td>{data.transactionType}</td>
                        <td>{data.accountName}</td>
                        <td>{this.getConvertedDate(data.timeStamp)}</td>
                        <td>{this.formatAMPM(data.timeStamp)}</td>
                        <td>
                            <Button color="info" onClick={() => { this.toggleModal("edit", data._id, data.amount); }}>Edit</Button>&nbsp;&nbsp;
                            <Button color="danger" onClick={() => {
                                    deleteTransactionsDetails({id:data._id}).then(({data})=>{
                                        console.log(data.msg)
                                    });
                                    }}>
                                        Delete
                            </Button>
                        </td>
                    </tr>
                )
            })
            )
        }
        else{
            return(
               <h4 className="text-center text-danger">No Transactions Happened . Please do transaction</h4>
            )
        }
    }

    addTransactions = () => {
        console.log("clicked")
        if (localStorage.getItem('token')) {
           // console.log("if")
            return (
                this.props.history.push('/add-transaction')
            )
        }
        else {
            return (
                console.log('Not Authorized User')
            )
        }
    }

    

    render() {

        return (
            <>
                <Base title={this.state.title}>
                    <Row>
                        <Col>
                        {/* <Button onClick={() => { this.toggleModal("new"); }} className="float-right mb-4" size="lg" outline color="success">Add new Account</Button> */}
                        {/* <Button onClick={ this.addUser} className="float-right mb-4 mr-2" size="lg" outline color="info">Add User to Account</Button> */}
                        <div className="float-left mb-2">
                               <span className="text-info">Choose Account:</span> 
                                <select
                                    className="form-control ml-2"
                                    style={{ display: "inline", width: "150px" }}
                                   value={this.state.accountName}
                                    onChange={(e) => {
                                        e.preventDefault();
                                        this.setState({accountName:e.target.value})
                                       setTimeout(()=>{
                                            this.fetchTransactionDetails();
                                       },100)
                                    }}
                                >
                                    
                                    {this.state.accountDetails.map((e, i) => {
                                        
                                    return (
                                        <option key={i} value={e.name}>
                                        {e.name}
                                        </option>
                                    );
                                    })}
                                </select>
                             </div>
                            <Table hover responsive>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Amount</th>
                                        <th>Transaction_Type</th>
                                        <th>AccountName</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Operations</th>
                                    </tr>
                                </thead>
                                <tbody  className="text-white">
                                    {this.renderTableData()}
                                </tbody>
                            </Table>
                            <br></br>
                            <Button onClick={this.addTransactions} outline color="info" size="lg">Continue Transactions</Button>
                            {/* <Link className="btn btn-secondary" to="/dashboard">
                              Add Transaction
                        </Link> */}
                        </Col>
                    </Row>
                    <Modal centered isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                        <ModalHeader toggle={this.toggleModal}>{this.state.modalData.header}</ModalHeader>
                        <ModalBody>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Account Name"
                                value={this.state.modalData.input}
                                onChange={(e) => {
                                    this.setState({ modalData:{...this.state.modalData, input: e.target.value} });
                                }}
                            />
                        </ModalBody>
                        <ModalFooter>
                            <Button
                                color="primary"
                                onClick={() => {
                                    this.toggleModal(null);
                                    this.saveData();
                                }}
                            >
                                Save
                            </Button>{" "}
                            <Button color="secondary" onClick={this.toggleModal}>
                                Cancel
                            </Button>
                        </ModalFooter>
                    </Modal>
                </Base>
            </>
        )
    }
}

export default listofAllTransaction;