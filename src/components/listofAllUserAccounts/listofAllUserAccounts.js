import React, { Component, Fragment } from 'react'
import Base from '../Base/base';
import { Row, Col, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import { getAllAccountDetails, addAccount, editAccount ,deleteAccount} from '../../auth/helper/dashboardHelper';

import AccountDetails from "../AccountDetails/accountDetails";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

let userId = '';
class listofAllUserAccounts extends Component {
    acctName = [];
    acctBalance = [];
    accountDetailsId=''
    constructor(props) {
        super();
        this.state = {
            accountDetails: [
                
            ],
            isModalOpen: false,
            modalData: [
                {
                    input: "",
                    id: '',
                    header: '',
                    type: ''
                }],
            
            isAccountDetailsModalOpen:false,
        }
    }
    componentDidMount() {
        const userdata = localStorage.getItem('userDetails');
        const udata = JSON.parse(userdata);
         userId = udata._id
        // console.log(userId)
        getAllAccountDetails().then(({ data }) => {
           //console.log(data)
            this.setState({
                accountDetails: data
            })
            //console.log(this.state.accountDetails)
        })

    }

    toggleAccountDetailsModal = (id = null) => {
       // console.log(id)
        //this.setState({accountDetailsId:id})
        this.accountDetailsId=id;
        this.setState({isAccountDetailsModalOpen:!this.state.isAccountDetailsModalOpen});
       // console.log(this.accountDetailsId)
      };

    toggleModal = (type, id = null, name = null) => {
        //console.log("toggle")
        if (type === "new") {
            this.setState({
                modalData: {
                    type: "new",
                    id: id,
                    header: "Add New Account",
                    input: name,
                }

            })
        }
        else if (type === "edit") {
            this.setState({
                modalData: {
                    type: "edit",
                    id: id,
                    header: "Edit Account Name",
                    input: name
                }

            })
        }
        this.setState({ isModalOpen: !this.state.isModalOpen });
    }
    saveData = () => {
       
         if(this.state.modalData.input === "" || this.state.modalData.input === null){
            // console.log("hi")
            toast("Fields Shouldn't be empty!! Fill it!", {
                type: "error"
              });
         }
         else{
              if(this.state.modalData.type === "new"){
             
                const obj = {
                    userId:userId,
                    name:this.state.modalData.input
                }
                addAccount({obj}).then((data)=>{
                    console.log(data);
                }).catch((err)=>{
                    console.log(err);
                })
            }
            else if(this.state.modalData.type === "edit"){
                var obj = {
                    id:this.state.modalData.id,
                    name:this.state.modalData.input
                }
              
               editAccount({obj}).then(({data})=>{
                   //console.log(data);
                   window.location.reload();
               })
            }
         }
    }

  

    renderTableData() {
        //console.log(this.state.accountDetails)
        return (this.state.accountDetails !== undefined && this.state.accountDetails.map((data, index) => {
        //  console.log(data)
            // const {name,balance } = data //destructuring
            return (
                <tr key={index}>
                    <td>{index+1}</td>
                    <td>
                        {data.name}
                        <span>
                            {data.userId._id !== userId
                                ? " (" + data.userId.name + " added you into thier account!) "
                                : ""}
                        </span>
                    </td>
                   
                   
                    <td>
                        <Button size="sm" outline color="primary" onClick={() => {this.toggleAccountDetailsModal(data._id);}}>View</Button>&nbsp;
                        <Button color="info" disabled={data.userId._id !== userId} onClick={() => { this.toggleModal("edit", data._id, data.name); }}>Edit</Button>&nbsp;&nbsp;
                        <Button color="danger" disabled={data.userId._id !== userId} onClick={() => {
                                deleteAccount({id:data._id});
                                }}>
                                    Delete
                        </Button>
                    </td>
                </tr>
            )
        })
        )
    }

    addTransactions = () => {
        console.log("clicked")
        if (localStorage.getItem('token')) {
           // console.log("if")
            return (
                this.props.history.push('/add-transaction')
            )
        }
        else {
            return (
                console.log('Not Authorized User')
            )
        }
    }

    addUser = ()=>{
        console.log("hi")
        return(
            this.props.history.push("/addUser")
        )
    }

    render() {

        return (
            <>
                <Base title="List Of User Accounts">
                    <ToastContainer></ToastContainer>
                    <Row>
                        <Col>
                        <Button onClick={() => { this.toggleModal("new"); }} className="float-right mb-4" size="lg" outline color="success">Add new Account</Button>
                        <Button onClick={ this.addUser} className="float-right mb-4 mr-2" size="lg" outline color="info">Add User to Account</Button>
                            <Table dark>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Account Name</th>
                                       
                                        <th>Operations</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.renderTableData()}
                                </tbody>
                            </Table>
                            <br></br>
                            <Button onClick={this.addTransactions} outline color="info" size="lg">Continue Transactions</Button>
                            {/* <Link className="btn btn-secondary" to="/dashboard">
                              Add Transaction
                        </Link> */}
                        </Col>
                    </Row>
                    <Modal centered isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                        <ModalHeader toggle={this.toggleModal}>{this.state.modalData.header}</ModalHeader>
                        <ModalBody>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Account Name"
                                value={this.state.modalData.input}
                                onChange={(e) => {
                                    e.preventDefault();
                                    this.setState({ modalData:{...this.state.modalData, input: e.target.value} });
                                }}
                            />
                        </ModalBody>
                        <ModalFooter>
                            <Button
                                color="primary"
                                onClick={() => {
                                    this.toggleModal(null);
                                    this.saveData();
                                }}
                            >
                                Save
                            </Button>{" "}
                            <Button color="secondary" onClick={this.toggleModal}>
                                Cancel
                            </Button>
                        </ModalFooter>
                    </Modal>

                     {/* show account details modal */}
                        <Modal
                            size="xl"
                            isOpen={this.state.isAccountDetailsModalOpen}
                            toggle={() => {
                           this.toggleAccountDetailsModal("");
                            }}
                         >
                            <ModalHeader
                            toggle={() => {
                                this.toggleAccountDetailsModal("");
                            }}
                            >
                            Account Details
                            </ModalHeader>
                            <ModalBody>
                            <AccountDetails accountId={this.accountDetailsId} />
                            </ModalBody>
                        </Modal>
                </Base>
            </>
        )
    }
}

export default listofAllUserAccounts;