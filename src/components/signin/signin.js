import React,{useState,Fragment} from 'react';
import Base from '../Base/base';
import {Link,Redirect} from 'react-router-dom';
import { Form, FormGroup, Label, Input, FormFeedback, FormText, Container,Button, } from 'reactstrap';
import { signIn } from '../../auth/helper/signupsignin';
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Signin  = (props)=>{
    const [values,setValues] =useState({
        email:"",
        password:"",
        error:"",
        loading:"",
        didRedirect:false
    });
    const {email,password,error,loading,didRedirect} = values;

    

    const changeHandler = name=>event=>{
        setValues({...values,error:false,[name]:event.target.value});
    }

    const onSubmit = (event)=>{
        event.preventDefault();
        setValues({...values,error:false,loading:true})
        
        if(email.length === 0){
            return (
                toast("Enter Your Email. Fields empty not acceptable!", {
                    type: "error"
                  })
            )
        }
        if(password.length === 0){
            return (
                toast("Enter Your Password. Fields empty not acceptable!", {
                    type: "error"
                  })
            )
        }
        //console.log(values);
        signIn({email,password})
        .then(({data})=>{
            //console.log(data)
            if(data.message === "Email Not Exists"){
                setValues({...values,error:data.msg,loading:true,didRedirect:false});
               
                toast(data.message, {
                    type: "error"
                  });
            }
            if(data.message === "Invalid Passsword" ){
                setValues({...values,error:data.msg,loading:true,didRedirect:false})
                toast(data.message, {
                    type: "error"
                  });
              
            }
            if(data.message === "Login Success"){
                //console.log(data.user)
                toast(data.message, {
                    type: "success"
                  });
                localStorage.setItem("token",data.token);
                localStorage.setItem("userDetails",JSON.stringify(data.user));
                setValues({...values,email:"",password:"",error:"",loading:"",didRedirect:true})
            }
        }).catch((error)=>{console.log(error)})

    }

    const performRedirect = ()=>{
        
        setTimeout(()=>{
            console.log("hi")
            if(didRedirect){
                return (
                    props.history.push("/listofaccount")
                )
            }
        },1000)
    }

    
    const SigninForm = ()=>{
        return(
            <Container>
            <Fragment>
            <Form>
                <FormGroup>
                    <Label for="email">Email</Label>
                    <Input type="email" name="email" id="email" value={email} placeholder="Enter your email" onChange={changeHandler("email")} />
                </FormGroup>
                <FormGroup>
                    <Label for="password">Password</Label>
                    <Input type="password" name="password" id="password" value={password} placeholder="Enter password" onChange={changeHandler("password")} />
                </FormGroup>
                
                <Button color="success" onClick={onSubmit} className="btn btn-block">Submit</Button>
            </Form>
        </Fragment>
        </Container>
        )
    }
    return(
        <>
       <Base title="Signin page" description="A page for user to sign in!">
            <ToastContainer></ToastContainer>
                {SigninForm()}
                {performRedirect()}
                {/* <p className="text-white text-center">{JSON.stringify(values)}</p> */}
            </Base>
            
        </>
    )
}

export default Signin;