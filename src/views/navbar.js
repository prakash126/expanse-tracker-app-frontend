import React,{useState} from 'react';
import { Fragment } from 'react';
import {Link, withRouter } from 'react-router-dom';
import {signOut} from '../auth/helper/signupsignin'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavbarText,
  
 
} from 'reactstrap';

const currentTab = (history,path)=>{
  if(history.location.pathname===path){
      return {color:"#2ecc72"}
  }
  else{
      return {color:"#FFFFFF"}
  }
}
let uname = "";
const NavbarComp = ({history})=>{
  if(localStorage.getItem('token')){
    const userdata = localStorage.getItem('userDetails');
    const udata = JSON.parse(userdata);
    uname = udata.name;
  }
  const [collapsed, setCollapsed] = useState(true);

  const toggleNavbar = () => setCollapsed(!collapsed);

  return(
    
        <Fragment>
      <Navbar color="primary" light expand="md">
        <NavbarBrand  className="text-white mr-4">Expanse Managing App</NavbarBrand>
        <NavbarToggler onClick={toggleNavbar} className="mr-2"/>
        <Collapse isOpen={!collapsed} navbar>
          <Nav className="mr-auto" navbar style={{cursor:"pointer"}}>  
                       
            {localStorage.getItem('token')?
                <Fragment>
                    <NavItem>
                          <Link style={currentTab(history,"/")} className="nav-link" to="/listofaccount">
                               UserAccounts
                           </Link>
                    </NavItem>
                  
                    <NavItem>
                            <Link style={currentTab(history,"/")} className="nav-link" to="/add-transaction">
                               AddTransaction
                           </Link>
                    </NavItem>
                    <NavItem>
                            <Link style={currentTab(history,"/")} className="nav-link" to="/get-transactionDetails">
                               TransactionDetails
                           </Link>
                    </NavItem>
                    <NavItem>
                          <Link style={currentTab(history,"/")} className="nav-link" to="/addUser">
                              AddUser
                          </Link>
                    </NavItem>
                    <NavItem>
                        
                          <span className="nav-link text-warning" style={{cursor:"pointer"}} onClick={()=>{
                              signOut(()=>{
                                  history.push("/")
                              })
                          }}>
                              Logout
                          </span>
                     
                    </NavItem>
                </Fragment>
            :<Fragment>
                 <NavItem>
                      <Link style={currentTab(history,"/signin")} className="nav-link" to="/signin">
                            Signin
                      </Link>
                 </NavItem>
                 <NavItem>
                          <Link style={currentTab(history,"/signup")} className="nav-link" to="/signup">
                              Signup
                        </Link>
                       
                 </NavItem>
             </Fragment>}
          </Nav>
            <NavbarText>{uname}</NavbarText>
        </Collapse>
      </Navbar>
    </Fragment>
    
  );
}

 

export default withRouter(NavbarComp)


// mittal.sarvaiya@jadeglobal.com 

                          